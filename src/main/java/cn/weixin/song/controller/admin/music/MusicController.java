package cn.weixin.song.controller.admin.music;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import cn.weixin.song.model.Music;
import cn.weixin.song.util.Pinyin4j;

import com.jcbase.core.auth.anno.RequiresPermissions;
import com.jcbase.core.controller.JCBaseController;
import com.jcbase.core.util.DateUtils;
import com.jcbase.core.util.FileUtils;
import com.jcbase.core.util.JqGridModelUtils;
import com.jcbase.core.util.RandomUtils;
import com.jcbase.core.view.InvokeResult;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.upload.UploadFile;
/**
 * 音乐管理
 * @author eason
 *
 */
public class MusicController extends JCBaseController {
	
	 @RequiresPermissions(value={"/music"})
	 public void index(){
		 render("music_index.jsp");
	 }
	 
	 @RequiresPermissions(value={"/music"})
	 public void getListData() {
		Page<Music> pageInfo=Music.dao.getPage(this.getPage(), this.getRows(),this.getOrderby());
		this.renderJson(JqGridModelUtils.toJqGridView(pageInfo)); 
	 }
	 
	 @RequiresPermissions(value={"/music"})
	 public void add() {
			Integer id=this.getParaToInt("id");
			if(id!=null){
				this.setAttr("item", Music.dao.findById(id));
			}
			this.setAttr("id", id);
			render("music_add.jsp");
		}
	 
	 @RequiresPermissions(value={"/music"})
	 public void save() {
		 	Integer id=getParaToInt("id");
			String name=getPara("name");
			String hard_level=getPara("hard_level");
			String music_url=getPara("music_url");
			String language=getPara("language");
			String singer=getPara("singer");
			InvokeResult result=Music.dao.saveMusic(id, name, hard_level, music_url,
					language,singer);
			this.renderJson(result);
		}
	 
	 @RequiresPermissions(value={"/music"})
	 public void uploadMusic(){
		 	UploadFile filedata=this.getFile();
				 if(filedata!=null){
			    		if(filedata.getFile().length()>1024*1024*2+100){
			    			this.renderJson(InvokeResult.failure("文件过大"));
			        		return;
			    		}
			    		String dateStr=DateUtils.format(new Date(), "yyyyMMdd");
			    		String relativefolder="music/"+dateStr;
			    		String mname=filedata.getFile().getName().split("\\.")[0];
			    		String ofileName=Pinyin4j.getOnePinyin(mname);
			    		if(ofileName==null)ofileName=DateUtils.getLongTime()+RandomUtils.getRandomString(8);
			    		String ext=FileUtils.getExtensionName(filedata.getFileName());
			    		String key=relativefolder+"/"+ofileName+ext;
			    		try {
			    			FileUtils.mkdir(PropKit.get("uploadPath")+key, false);
			    			FileUtils.copy(filedata.getFile(), new File(PropKit.get("uploadPath")+key), BUFFER_SIZE);
						} catch (Exception e) {
							e.printStackTrace();
							this.renderJson(InvokeResult.failure("网络缓慢,上传失败"));
							return;
						}
			    	   filedata.getFile().delete();
			    	   Map<String,Object> item=new HashMap<String,Object>();
			    	   item.put("musicUrl",key);
			    	   item.put("name", mname);
			    	   this.renderJson(item);
			    	   return;
			    	}
			 	this.renderJson(InvokeResult.failure("上传失败"));
	    }
	 
}