<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="apple-touch-fullscreen" content="YES" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="viewport" content="width=device-width, initial-scale=1.0,  minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>${activity.title}</title>
<link href="${res_url}wxcj/style.css" rel="stylesheet" type="text/css">
<link href="${res_url}wxcj/webBox.css" rel="stylesheet"/>
<link href="${res_url}wxcj/msgBox.css" rel="stylesheet"/>
<script type="text/javascript" src="http://libs.baidu.com/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="${res_url}wxcj/js/awardRotate.js"></script>
<script type="text/javascript" src="${res_url}wxcj/js/command.js"></script>
<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.1.0.js"></script>
<script type="text/javascript">
var turnplate={
		restaraunts:[],				//大转盘奖品名称
		colors:[],					//大转盘奖品区块对应背景颜色
		outsideRadius:192,			//大转盘外圆的半径
		textRadius:155,				//大转盘奖品位置距离圆心的距离
		insideRadius:68,			//大转盘内圆的半径
		startAngle:0,				//开始角度
		
		bRotate:false				//false:停止;ture:旋转
};

$(document).ready(function(){
	//动态添加大转盘的奖品与奖品区域背景颜色
	turnplate.restaraunts = ${awardNames}; 
	turnplate.colors = ${colors};
	var rotateTimeOut = function (){
		$('#wheelcanvas').rotate({
			angle:0,
			animateTo:2160,
			duration:8000,
			callback:function (){ 
				alert('网络超时，请检查您的网络设置！');
			}
		});
	};

	//旋转转盘 item:奖品位置; txt：提示语;
	var rotateFn = function (item, txt){
		var angles = item * (360 / turnplate.restaraunts.length) - (360 / (turnplate.restaraunts.length*2));
		if(angles<270){
			angles = 270 - angles; 
		}else{
			angles = 360 - angles + 270;
		} 
		$('#wheelcanvas').stopRotate();
		$('#wheelcanvas').rotate({
			angle:0,
			animateTo:angles+1800,
			duration:8000,
			callback:function (){
				document.getElementById('winScore01Id').innerHTML=txt;
				document.getElementById('gameWinId').style.opacity=1;
				document.getElementById('gameWinId').style.webkitTransform='scale(1)';
				document.getElementById('submitUserInfoId').onclick=function(){
					submitUserInfo();
				}
				//document.getElementById('prizeId').innerHTML=txt;
				//document.getElementById('yeslotteryId').style.opacity=1;
				//document.getElementById('yeslotteryId').style.webkitTransform='scale(1)';
				turnplate.bRotate = !turnplate.bRotate;
			}
		});
	};
	function submitUserInfo(){
		var mobileNumber=document.getElementById('mobileNumberId').value;
		if(!isMobileNumber(mobileNumber)){
			msgBox('请输入正确的手机号码！');
			return false;
		}
		var postData={openid:"${openid}",mobilephone:mobileNumber,appId:"${app.id}"};
	    $.post("/wx/luck/submitInfo", postData, function(rs){
	      	 if (rs.code == 0) {
		      		document.getElementById('gameWinId').style.display='none';
		    		document.getElementById('prizeId').innerHTML=$("#winScore01Id").html();
		    		document.getElementById('yeslotteryId').style.opacity=1;
		    		document.getElementById('yeslotteryId').style.webkitTransform='scale(1)';
               } else {
            	   msgBox(rs.msg);
               }
	      }, 'json');
	}
	$('.pointer').click(function (){
		if(turnplate.bRotate)return;
		turnplate.bRotate = !turnplate.bRotate;
		//获取随机数(奖品个数范围内)
		var item = 1;
		var postData={openid:"${openid}",aid:"${activity.id}"};
	    $.post("/wx/luck/lottery", postData, function(rs){
	      	 if (rs.code == 0) {
	      			rotateFn(rs.data.item, turnplate.restaraunts[rs.data.item-1]);
	               } else {
	            	   if(rs.code==10001){//未关注，弹出提示层
	            		   document.getElementById('wx_account_div').style.opacity=1;
		   		    	   document.getElementById('wx_account_div').style.webkitTransform='scale(1)';
	            	   }else{
	            	   		msgBox(rs.msg);
	            	   }
	               }
	      	turnplate.bRotate = !turnplate.bRotate;
	      }, 'json');
	});
});

function rnd(n, m){
	var random = Math.floor(Math.random()*(m-n+1)+n);
	return random;
	
}
function shareWeixin(){
	var shareImg='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANUAAAByCAYAAADEWZTVAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjE0QTk2MzgzRTU2RjExRTM5RTQ1QjgxOUEyRTU2NjdGIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjE0QTk2Mzg0RTU2RjExRTM5RTQ1QjgxOUEyRTU2NjdGIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MTRBOTYzODFFNTZGMTFFMzlFNDVCODE5QTJFNTY2N0YiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MTRBOTYzODJFNTZGMTFFMzlFNDVCODE5QTJFNTY2N0YiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz530a++AAAUA0lEQVR42uxda4g8y1WvKxtf8aod31w12v/4+OC712hULgiz4oOAEXsVYxAi9vpFEFF6RVHRfJgR8lHjjH6IEBVnRGIU85hBMcFwxW1QQqJJmJaEC4EYd4KaXEV0rc6ck/3t2aru6sfc/8zs+cFhZmdnuqtP1a/q1Kmqc564ubkxCsUR4WVWXmzls6183MpHrbzLygeerwI8oaRSHBkKK9/i+PyDRK7q9cNWPkKysfKfJP8Bf/+Pkkqh2CKy8vNWvtvKV1v5vI7XqQj2Nis/rKRSKO7ihVa+2MoXWfkCK19i5QtJ8H31v8+y8gL47Y9YmSupFIpueAGR6GesvNTKG628Qs0/haI9PtPKT1v5WStfRp9VDo6vsfJslwueqE4VDxSfZuXCyi+SeYh4bVdC6UileIioBpKftPJLMDIhKs/fV1j5t643+BTVseIBofII/qOV37HyIit/aOXcyvfAd97Qh1BKKsVDwZdbWVj5Kyufa+XnrDxl5ZX0+Rl89/f63kzNP8Wxm3oVgX7Nyn9b+Q0rr7PynBhYPkgke6+Vrx3ipgrFMaIix+utfKuV37Xyy2a7g8JlEj5F7/98iBur+ac4RvyElb832/1/T5uty/wjnu/+OLz/syFuruaf4phQ7Yj4LSuvsvLbVn5BmHoSlVv9w0S+yjlR7br4XzX/FIotEit/ZOVJKz9g5c0Bv/l+IlSF5RCEUvNPcQx4gpwRf0sOh28IJFSFH4P3y8EKpOaf4oBR7UD/fRpxftNsF3RDR5snyfT7dPq7Wgh+dohCqfmnOFR8B5l71VGP6njGn7b8/fcCod4zFKHU/FMcqrmXW/kb+vs7OxDK0LyL8bYhC6gjleKQ8DlW/oAIUZ3wfbmVD3UcTL4P/n7roKzXOZXiQFBtNfoLK19v5U3kZPhYx2t9m5Vn6P1zNDd7Ts0/xUPCN1r5OyLU6638UA9CVcC9fu8YklBKKsUhoHKRr8z2zFN1zunVpv960im8f8vQBVZSKfYZX0eE+nwrrzHbgC5DzFe+Cd6/eehC65xKsa94iuY9X2rl16386oDXrnasf6qV95ntsXkdqRRHjyoIyx8ToV43MKEqsNf7jbsovJJKsY/4FbNdf3q72UY3Ghr/Tq9v2EXh1fxT7BsqT191bOP/zNbb9/4d3OOfzXaL0tO7eABd/FXsG15L5t9rdkSoCn9p5U929QA6Uin2CVVygXeabcjlarH3o4f4EDqnUuwTcnqdHiqhdKRS7BOq9Del2W6YfQm9P0joSKXYF7ya2uMzh0woJZVin/Cj9Do/9AdR80+xD6jCif0Tva+2Jr1bRyqFoh9+kF4/dOiEUlIp9gUvp9dnjuFhlFSKx40q9t5L6f0/KKkUiv6otiLxzp53KakUiv74Znj/ASWVQjEsqZ5VUikU/fFieP+vSiqFoj843251GvdGSaVQ9Mdn0Ot/HcsDKakUjxtP0OuTSiqFYhh8DNrik0oqhaI/0I3+SEmlUPQH7qJ4WkmlUPTHm+D9K45ikqhHPxR7gCrxAKe2+S6zzYqopFIoeuArzXbf3wvNNoJStcH2YGNUqPmn2Af8i9ke//i4la+ikeplSiqFoh/+2sq3kylY7bL4KTX/FAqFkkqhUFIpFEoqhUJJpVAolFQKhZJKoThq6DrVsBipChQPgVSZuU3RskskZhsHPNJm9ckOZld637WO4z51+RBIlZCS5GeZ4/O+99mQ7Kqir3bQoGIq+9CEmu9IF2OSXYKv36n8Dyk9aUqVPaKGVKVrKQYm1WqH5Z/uiLQpdTCPBiTpnHQbU7ldmJhuKXN2PUpl1EZOu17goZAqo8ZTVfSMGn8XQuV0Le7FCtE7F/B/Q41mCKKNibSnO9BNbIbLB1VdawnEj2osh9mOR5oNEbdt+ccewsehndpDIVVVgZcD9PIraiiRp0KMMKUiGgE2HRsoX68i8wVdL6t5xq4oByRU1bGc14wylQm7GMBKiIX5igQqaISctdT9mH47cXSmY0GqQrw39FybYyJVSpWW0EOW0FjigcymwtMYUpJTcZ8o4L4xVdrI02jQ/DOe3nLDFfoY9T9vIBTOVS4DzenMoZdIdC6FQ8+so4z0EtKB8L3OPKZqAXUyousndO8RkPviWEjFdvwlPdgIFLCBRlmC2bYZ2OwpPY29zSiIZSqpzIVohJvHTB5TM0daNDgvMiJdSPk3oBfUbUp/T2AUSc19T2PkcWqcOjpGNvtm4n+RKAeOXBckOAJ/ou6OZfG3UsqaTC0cnVC4R0Hz7bKD3e3rpTeg5KEcE9xz7jIHLnsUV0SKXThbWPcJ3SOBz2NBkiYs6fsXYgS/APImRBC+Po9CC08dzel7p2KkXFInsBKdw1yY9Qnp8ROEPbY5VSx6+tJTwREpbChPUuJpFEjqosU8IiO5MLtPKs2EiqFzYHKtWlxjQw0uFc9uREeWOUy1uAWp4oYRcSxMYbZQTj26dJl9rIuZQwcjsCrws096k4+FVEygkHUnNJ/KARokjoRj6JVxXlQ6TIsmD9SMXnNz661sQ8y2c0V0hLCpxt7LWY2uuEd/JPSPDQ89pdJ8XTeQxHfPNYwsG2jcSLpNw0gfkX5l3YzpN5ee+fPMQbTFQ/P++UaQVSBxIpiUJoJI2AMbMKNKIV1MSTaV2AMYw/VW8Lrp0SlEosOZkCDBogZCbsDk8o28Ln2zTkM9l1yWlMq4MXeXLVLQRwzf5/czMSKi650JmZr7yxcJtBtZ3kg++zGaf6E9XciEPyXzIYLvs2dxAZXnWjyNOjT2qTCZpAkWw5whBZKd9iRV4RnRJ4FmWWnqF78zjzk5Mndd002jN5MEnQ0Lc7uon9K11tAR8Og+EyMijsZrYYqei45wDB4/HMlwfrg6RlKtanrUGHoidn2WAZXJZlEBc4aRmOxOPY3pytEzNs2jUrr23NNIS9FLLgfyBA6xfpdQY0s9ZnkCTgVu5GmD6RfBNVfU2JdivpQJAvDCLXc6ZzXPPIO50Ib07vJinpnbdbjS3HXpJ7JjOEbzLxGCHiYeaSaBcxNpumWe3tZlvizATAmdR03AdGza9sRmx0VPXQ01p808c6/IQ1zu4GY1OmE3NXvg5JrVJZBoCZ0Om3FnAR0mmt28fIHeyksof0R/F2JOePcZKpf6kUhmJbeytLK2Mrcyps9j8d2EXiMrU3ptun71nWv4re8zltHNFnHAta+oHPz3mJ6j6XmvA8ted431ALqv0wP/D/UQQ101lS+kvAnpekx/L4U+mySn6+bUbq5JpuI7V546vvPcx0QqKVXFpZ7/raHCrkiRIYqXSk0bGjZXVN11xx2ua6jM8wE6ovVA+r5yPCuT54Ya6Jrkhp4vH7C8OV1z3rKzGdH3b+D3qeP3S0d5c1d5jsX8GznWiniHdCFMElw34kXDJZgvdU6LhcNEcU20I5i3NZmAC8d9Vy5Xrbj+yPRfbJZercSxXCDnKnXmFG65ioXZvRHXmLfw+kVgfvnA1+brtpkn4rrcRjizStDNZUCbOApSxTDBlA2Tbf1LQUDcDsRzrDH8xkVal/3P858RuNpHYh4RNXgCfZ43nFfh2hc29lUgcRKxBJAI79/as44Xeg9+jtzcbpbltR8+rIh1kAd6/bjhjsFZUDR0rtyRRaZ+HyI+3wp0NQInxwTc7Uy4VNQpbyTg9jE5hm1KfELTNSnNSR4Jd6jrvMy8ZsGPj16ci0YqCbQCb1IBZbjs8FxjIFJi7u8UCd3ak4s1NHQubBwOma57C11bxfj+KTiHSnO7Uz3UM3oFrn/fdjBehD6j787ByVE2lDuF9amCyhaDo0su15SOTodd7OWhk4pJ49uCwr3wJYwycc3ajA+47hEJ4vRdgK0bYdizGJndb1caAleORpw7nicz7Y7ExGI0cJFK7glkC+aipq6nsLa1MHePpGRi6QV3hdTWxSGTil2uTQuUUc8Gz8M77mAojaKLrnMw/c46mvmuEW5EBDltWdc8Eg26ifiQScW7Ci61LR8c8cwORvbI7MmRGI37p1AoqRQKJZVCoaRSKBRKKoVCSaVQKKkUCoWSSqFQUikUSiqFQiGxD6l0OLpnrNXRiCHS3nCWj9Fj0Hn0EPTfZ6TiszNDbC6tO77RttIw8tGxgYOenPe4Bke+ZV1x/WHY6S6h1ULqZk11XPRoP64DqXul/5OGwvPhMFcD5QDyIWGJo5peKgElc/RS33d5i74PPOJJUhWOvxkhZ4n4TBAfaCseA2n5KELfdDoXok5kbDw8BInBclhPi473ZcJySiM+CtK2I+XDpGZf9e8bqThedNOxCg6ldVHzoNxDtRn6fSRdmfoj5NhQJPlXDnMndlz/zKPQFK7HxMTIsdwZjEWDxIQIfYJerj31wSYJnxO79JgteKzfdaaMn/HM3J7WXTgIh2fTulg3fJaKj+5cmvDjNKyHc0d74yM62GniQUysg80u9X/isbmnJiyQ4jkRa+55UO6hLoSp4Us0tjH9snHw72VsPB9ZYsfvfaNpCc+Bx65zqLQLIBceu47Es52b9nmT+Le5uXsqVY4qriMQHDgSSTU3dw93YtqZzKFHblhtGySmnzHmfprVMZj+TSMQl2tVY8XI+0lLqegw2rfS/4mjJ8lrCNKVWAtxj3EDKeRnZx17+bTBxCg7EhfjGkTQEY3N/ePe0szKG3pE7PkwxLQBvXFo6a5pgVZEsCWMTHyqdmzuZ8DAEXrl0XMEvTWWORZ1y/U4M/ej/obMp+rMz0mNXjnWxWLX+j/pSag2xEKCjcVcLPGYh2PT76g6B5+vC0fcN4UoxnlwZaTA0TOrMT84xkIE85cCrnk5kONA5u/CsNY8aqw8v3OVnSPPynxS0vGBidjyQEdDBkRFYk9FY2+KBMzRc+usr8H0f1JDqLYnNM8bemHpGMAIrKWHEMZ0P9nLFXLeYJpkgeYAm1CzGmeLbLA4Z5mZ+qizGGK6BB1EAxLKQO9rYJ6JnYFv1PCVvTDt4k1wp9oU8jkRpnUs2koJjo+megvxDwynfwp4eE1BBTEIIkfpXEME1SkFNEzpO12jo05ropNGFPGzS7BFGRU1bwi0OaYgiqOA8q7pmhw0MnIEzbwBPU1FAMnYE3CyLhhoHx2EBqG8Ij3EpIeM/p46njEbKPBml0CgS8dvRg2BM/MeOuysf/b+uSa3ONGOHa9G2MRt2JxRb3Vu/JkOu0wopSl7au6mv0EbWaYCPW9Y2+FRk82XDVx/DSbwI1Mf2SnUvGZP1tkAoxNHI3okPHkc56MEnURgwnHycV/4MSPMsbJmFEAPH89BeYRrWnti3V2I0Y3vWxeerqu3srP+T2pMvIVwnRto5BjRNIK5T2jhV+ACxbWQWQ+3JzagHNatDDQWTqdSwH0zE75LgaOscsYKbhClsMXrvGBFYKfgWhNxrfeNTLv8TnNwtoxAJ6wPXhzGtbiRaV4MjoULHs24VDwLm4DskRyb5iCZ0kkSGX/03z7+gd76D4lQO6UbnDm8ZoW5Da08a9HoudJcGdWxMmPTbcW9BPIU4Pp2zc+69GLobk7A5seItJgkbtOis+BOoYTesq5S2zzHCkYHXpcqHJP+FXgBz+EZTYMX1VWGK+PeObEQbvazBi+uKyVo3XNvOhKqt/5PAhibGv+uCV5UbLNthnP7YI4f37oRm1xttqTMHIoeYs9Z7BjVJuDmjcFMkutS7BBYBXZipXEvYBoY1eOWHZoMVcyjw1hcP6LnOjV3czAtGib5saMTrHMWrWBEuKzpbHxx4zPj39kyM7cx3dsSq7/+A1K1jBomnMuBM3XENEm+HiCrBToarnr+/hpS9ExpMp9ACpk1fTbyOHFCnBS5I+2MT0/rFvqJRVofzjTCDp3MMTnPhNNnFHD9TDiLmp55LRwoLoeDKwOKdEQldG90EF2Dc4Pvkzwf+j+pMaHG5nYLie87TYm12iKCdZML0y3BclIzn5nCfRIYCUPus3D0lqkwf3wZ3XmeUQSYHZzLdmnu7jWUTofCdN9YG5vb3SHsmEhFGXmhNAmYC6J7G2PNb2pG0ZEwmdlEf+SZT40d7vUVjFojczc0cwHz3JD9msPpvyZfT97BxWkGGA3Qlcvu9RwSetW50a+gV0K5hkRgU+iJsxZlmzqSxnHistiR4C2H/FhJQM4kqU/Oh3UDPf6InmfawQJwJaDjUSCFHE0pJFFLA5OzYa6mKTx7FJDo7hruMfYkdGMX/xzqk+HLAdY299Zg+vcNa7lHabEwDeOBCJVS4XNYJ1mC8ro0JNNijSVqeJYpVHgK5tBSKNuXhCxvqOA6s2NE90ZdpC3XCH2kykE3OSTAW4NZOA/o0ELWDX3PO4U10bjFGmQKJl8OnW8C9RRKqkH1f+IZ1iY1K9y8VtP3LFUEJkACQ/zG3M0oXpjhdhPg3i65+9o07AwowQyMYZlh4vFSTsh04MwSswbTw7fOh7vrJzBBRk9dn21WsdA7JmfDc09t9ZyAOz0G3UZgZnE6oLRFPWfGfQQlg/VP3GLU5LkcXP949OMqYBE0hwo4bdnYY1CwK69TCmtAmwHIg6TNzP0EbJhPauNx76NupFcRc19FgnguvRU91kzkImYE8whenqjbdIyLtxvRkcl1JZlRMa5Z9HXV78i4j8esxBLNqXjGVeAaXlNuqwTKkJiw836D6p9JlcJiWRnQU0Qt3dx4bADT0aAbNjX3M4a3XZ9Kxa4MF3lWHRaXM3M3kRs+1y4PK4acV0uMeyOvLOfS3KZilaejcYKPO024YwpxRi2hZy+MP23ruqMTip/1ighZmN2jk/6fz8AvrgaYw8jFw+mkR48S9yTPPiGH9Zkhj7XLLWbPN7qsHcnRcLLP+v9/AQYALvv0IQBfItYAAAAASUVORK5CYII=';
	var bodyObj = document.body;//页面body节点对象
	var loadingBox = document.createElement("div");//Loading对象
	bodyObj.appendChild(loadingBox);//新建的对象加入页面
	loadingBox.innerHTML='<div style="position: fixed;top:5px;right:10px; "><img src="'+shareImg+'" /></div>';
	loadingBox.id="loadingBox_by_gzy_weixin";
	loadingBox.style.zIndex='9999999';
	loadingBox.className="loadingBox";
	loadingBox.style.height=document.documentElement.clientHeight+'px';
	loadingBox.onclick=function(){
		closeShareWeixinBox();
	}
	function closeShareWeixinBox(){
		bodyObj.removeChild(loadingBox);
	}
	
}

//页面所有元素加载完毕后执行drawRouletteWheel()方法对转盘进行渲染
window.onload=function(){
	drawRouletteWheel();
	document.getElementById('infoBtnId02').onclick=function(){
		shareWeixin();
	}
};

function drawRouletteWheel() {    
  var canvas = document.getElementById("wheelcanvas");    
  if (canvas.getContext) {
	  //根据奖品个数计算圆周角度
	  var arc = Math.PI / (turnplate.restaraunts.length/2);
	  var ctx = canvas.getContext("2d");
	  //在给定矩形内清空一个矩形
	  ctx.clearRect(0,0,422,422);
	  //strokeStyle 属性设置或返回用于笔触的颜色、渐变或模式  
	  ctx.strokeStyle = "#FFBE04";
	  //font 属性设置或返回画布上文本内容的当前字体属性
	  ctx.font = '16px Microsoft YaHei';      
	  for(var i = 0; i < turnplate.restaraunts.length; i++) {       
		  var angle = turnplate.startAngle + i * arc;
		  ctx.fillStyle = turnplate.colors[i];
		  ctx.beginPath();
		  //arc(x,y,r,起始角,结束角,绘制方向) 方法创建弧/曲线（用于创建圆或部分圆）    
		  ctx.arc(211, 211, turnplate.outsideRadius, angle, angle + arc, false);    
		  ctx.arc(211, 211, turnplate.insideRadius, angle + arc, angle, true);
		  ctx.stroke();  
		  ctx.fill();
		  //锁画布(为了保存之前的画布状态)
		  ctx.save();   
		  
		  //----绘制奖品开始----
		  ctx.fillStyle = "#E5302F";
		  var text = turnplate.restaraunts[i];
		  var line_height = 17;
		  //translate方法重新映射画布上的 (0,0) 位置
		  ctx.translate(211 + Math.cos(angle + arc / 2) * turnplate.textRadius, 211 + Math.sin(angle + arc / 2) * turnplate.textRadius);
		  
		  //rotate方法旋转当前的绘图
		  ctx.rotate(angle + arc / 2 + Math.PI / 2);
		  
		  /** 下面代码根据奖品类型、奖品名称长度渲染不同效果，如字体、颜色、图片效果。(具体根据实际情况改变) **/
		  if(text.indexOf("M")>0){//流量包
			  var texts = text.split("M");
			  for(var j = 0; j<texts.length; j++){
				  ctx.font = j == 0?'bold 20px Microsoft YaHei':'16px Microsoft YaHei';
				  if(j == 0){
					  ctx.fillText(texts[j]+"M", -ctx.measureText(texts[j]+"M").width / 2, j * line_height);
				  }else{
					  ctx.fillText(texts[j], -ctx.measureText(texts[j]).width / 2, j * line_height);
				  }
			  }
		  }else if(text.indexOf("M") == -1 && text.length>6){//奖品名称长度超过一定范围 
			  text = text.substring(0,6)+"||"+text.substring(6);
			  var texts = text.split("||");
			  for(var j = 0; j<texts.length; j++){
				  ctx.fillText(texts[j], -ctx.measureText(texts[j]).width / 2, j * line_height);
			  }
		  }else{
			  //在画布上绘制填色的文本。文本的默认颜色是黑色
			  //measureText()方法返回包含一个对象，该对象包含以像素计的指定字体宽度
			  ctx.fillText(text, -ctx.measureText(text).width / 2, 0);
		  }
		
		  //把当前画布返回（调整）到上一个save()状态之前 
		  ctx.restore();
		  //----绘制奖品结束----
	  }     
  } 
}

</script>
</head>
<body>
<div class="main" id="mainId">
	<div class="banner">
		<div class="turnplate" style="background-image:url(${res_url}wxcj/images/turnplate-bg.png);background-size:100% 100%;">
			<canvas class="item" id="wheelcanvas" width="422px" height="422px"></canvas>
			<img class="pointer" src="${res_url}wxcj/images/turnplate-pointer.png"/>
		</div>
	</div>
<div class="infoBox">
    	<div class="infoBoxCon">
        	<div class="title">
            	活动说明
            </div>
           	 <p class="f_orange">每个微信号可参与一次，100%中奖哦！</p>
           	 <p class="f_orange">活动日期：即日起至2016年11月11日23:59分结束</p>
           	  <p class="f_orange">参与方法：关注XXXX公众号，点击幸运转盘->玩转转盘菜单</p>
        </div>
    </div>
    
    <div class="h10"></div>

    <div class="infoBox">
    	<div class="infoBoxCon">
        	<div class="title">
            	优惠券使用说明
            </div>
           <p class="f_orange">到指定的商家门店出示您的优惠券给商家即可！</p>
           <p class="f_orange">查看我的优惠券入口：XXXX公众号，点击幸运转盘->查看优惠券菜单</p>
        </div>
    </div>
<div class="yeslottery" id="yeslotteryId">
    	<div class="infoBox">
            <h1 id="prizeId">奖品</h1>
            <div class="h10"></div>
            <p class="f12">将活动分享出去，还可以再玩一次喔！（分享至朋友圈或微信好友各增加一次抽奖机会）</p>
            <div class="h10"></div>
            <div class="infoBtn" id="infoBtnId02">
                我要分享
            </div>
            <div class="h10"></div>
        </div>
    </div>
    <div class="yeslottery" id="wx_account_div">
    	<div class="infoBox">
            <h1>${app.name}官方公众号</h1>
            <p><img width="200px" id="wx_qr_code_img" src="${static_url}${app.qrCodeUrl}" alt="" />  </p>
            <div class="h10"></div>
            <p class="f12">长按图片识别二维码即可参与</p>
            <div class="h10"></div>
        </div>
    </div>
    <div class="gameWin" id="gameWinId">
    	<div class="infoBox">
        	<h1>恭喜您获得<span id="winScore01Id"></span></h1>
            <div class="h10"></div>
            <p class="f12">输入您真实的手机号才可领取哦</p>
             <div class="h5"></div>
            <p>
            	<input type="text" name="mobileNumber" id="mobileNumberId" value="" placeholder="填写您的手机号" maxlength="11" />
            </p>
            <div class="h10"></div>
            <div class="infoBtn" id="submitUserInfoId">
            	提 交
            </div>
            <div class="h10"></div>
        </div>
    </div>
      <div class="h20"></div>
    </div>
    
    <script>


wx.config({
    debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
    appId: '${wxAppId}', // 必填，公众号的唯一标识
    timestamp: '${appData.timestamp}', // 必填，生成签名的时间戳
    nonceStr: '${appData.noncestr}', // 必填，生成签名的随机串
    signature: '${appData.signature}',// 必填，签名，见附录1
    jsApiList: ['onMenuShareTimeline','onMenuShareAppMessage','hideMenuItems'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
});
   //分享到朋友圈
	var shareTimeline={
	   title:"${activity.title}",
	   desc:"${activity.summary}",
	   imgUrl:"${static_url}activity/pic1.png",
	   link:"${wx_base_url}luck?aid=${activity.id}",
	   success: function (res) {
			var postData={openid:"${openid}",aid:"${activity.id}",type:1};
		    $.post("/wx/luck/share", postData, function(rs){
		    	if(rs.code==0){
		    		if(rs.data.add_count>0){
		    			msgBox("增加了"+rs.data.add_count+"次抽奖机会");
		    		}
		    	}
		      }, 'json');
	    },
	    cancel: function (res) {
	       
	    }
	};
   //发送给朋友
	var shareAppMessage={
	   title:"${activity.title}",
	   desc:"${activity.summary}",
	   imgUrl:"${static_url}activity/pic1.png",
	   link:"${wx_base_url}luck?aid=${activity.id}",
	   success: function (res) {
			var postData={openid:"${openid}",aid:"${activity.id}",type:2};
		    $.post("/wx/luck/share", postData, function(rs){
		    	if(rs.code==0){
		    		if(rs.data.add_count>0){
		    			msgBox("增加了"+rs.data.add_count+"次抽奖机会");
		    		}
		    	}
		      }, 'json');
	    },
	    cancel: function (res) { 
	    }
	};
	wx.ready(function(){
		wx.hideMenuItems({
		    menuList: ["menuItem:copyUrl","menuItem:openWithSafari","menuItem:openWithQQBrowser","menuItem:originPage","menuItem:share:QZone","menuItem:share:weiboApp","menuItem:share:qq"] // 要隐藏的菜单项，只能隐藏“传播类”和“保护类”按钮，所有menu项见附录3
		});
		wx.onMenuShareTimeline(shareTimeline);
		wx.onMenuShareAppMessage(shareAppMessage);
	}); 
</script>
</body>
</html>

