/**
 * Copyright (c) 2011-2016, Eason Pan(pylxyhome@vip.qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jcbase.conf;

import cn.weixin.song.controller.admin.ActivityLuckController;
import cn.weixin.song.controller.admin.ActivityShareRecordController;
import cn.weixin.song.controller.admin.ActivityUserPlayController;
import cn.weixin.song.controller.admin.CouponSnController;
import cn.weixin.song.controller.admin.music.MusicActivityController;
import cn.weixin.song.controller.admin.music.MusicController;

import com.jcbase.controller.ImageController;
import com.jcbase.controller.IndexController;
import com.jcbase.controller.app.AppVersionController;
import com.jcbase.controller.sys.DictController;
import com.jcbase.controller.sys.LogController;
import com.jcbase.controller.sys.ResController;
import com.jcbase.controller.sys.RoleController;
import com.jcbase.controller.sys.UserController;
import com.jfinal.config.Routes;
/**
 * 后台管理Routes配置
 * @author eason
 *
 */
public class AdminRoutes extends Routes{

	@Override
	public void config() {
		add("/", IndexController.class,"/WEB-INF/view");
		add("/sys/log", LogController.class,"/WEB-INF/view/sys");
		add("/sys/res", ResController.class,"/WEB-INF/view/sys");
		add("/sys/user", UserController.class,"/WEB-INF/view/sys");
		add("/sys/role", RoleController.class,"/WEB-INF/view/sys");
		add("/dict", DictController.class,"/WEB-INF/view/sys/dict");
		add("/app", AppVersionController.class,"/WEB-INF/view/app");
		add("/image", ImageController.class,"/WEB-INF/view/image");
		
		add("/activity/luck", ActivityLuckController.class,"/WEB-INF/view/activity");
		add("/activity/share", ActivityShareRecordController.class,"/WEB-INF/view/activity/share");
		add("/activity/user_play", ActivityUserPlayController.class,"/WEB-INF/view/activity/user_play");
		add("/coupon/sncode", CouponSnController.class,"/WEB-INF/view/coupon/sncode");
		
		add("/mactivity", MusicActivityController.class,"/WEB-INF/view/music");
		add("/music", MusicController.class,"/WEB-INF/view/music");
	}

}
